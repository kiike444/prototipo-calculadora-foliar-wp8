﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using AplicacionBiologia.Resources;
using System.Windows.Media.Imaging;
using OpenCVComponent;
using Microsoft.Xna.Framework.GamerServices;
using AplicacionBiologia.Util;
using System.Windows.Media;

namespace AplicacionBiologia
{
    public partial class MainPage : PhoneApplicationPage
    {
        private OpenCVLib opencv = new OpenCVLib();
        int numFormula = 0;
        int numImagen = 0;

        // Constructor
        public MainPage()
        {
            InitializeComponent();

            // Sample code to localize the ApplicationBar
            //BuildLocalizedApplicationBar();
        }


        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (Preview.Source != null)
            {
                ProcessButton.IsEnabled = false;

                if (numFormula == 4)
                {
                    numFormula = 0;
                }
                else {
                    numFormula++;
                }

                switch (numFormula) {
                    case 0:
                        tbFormula.Text = "Fórmula 1 : ((r + b) / 255 - (r - b) / 255) / 3";
                        numFormula = CalculosBiologia.FORMULA_1;
                        break;
                    case 1:
                        tbFormula.Text = "Fórmula 2 : b / (r + b + g)";
                        numFormula = CalculosBiologia.FORMULA_2;
                        break;
                    case 2:
                        tbFormula.Text = "Fórmula 3 : g / (r + b + g)";
                        numFormula = CalculosBiologia.FORMULA_3;
                        break;
                    case 3:
                        tbFormula.Text = "Fórmula 4 : r /  (r + b + g)";
                        numFormula = CalculosBiologia.FORMULA_4;
                        break;
                    case 4:
                        tbFormula.Text = "Fórmula 5 : g / r";
                        numFormula = CalculosBiologia.FORMULA_5;
                        break;
                };

                ProcessButton.IsEnabled = true;
            }
        }

        private void Button_Click2(object sender, RoutedEventArgs e)
        {
            if (Preview.Source != null)
            {
                ProcessButton2.IsEnabled = false;

                double area = CalculosBiologia.calcularArea(Preview.Source);

                List<string> MBOPTIONS = new List<string>();
                MBOPTIONS.Add("OK");
                string msg = "El área de la muestra es " + area + " cm2";
                Guide.BeginShowMessageBox("Área", msg, MBOPTIONS, 0, MessageBoxIcon.Alert, null, null);

                ProcessButton2.IsEnabled = true;
            }
        }

        private void Button_Click3(object sender, RoutedEventArgs e)
        {
            if (Preview.Source != null)
            {
                ProcessButton3.IsEnabled = false;

                double clorofila = CalculosBiologia.calcularClorofila(Preview.Source, numFormula);

                List<string> MBOPTIONS = new List<string>();
                MBOPTIONS.Add("OK");
                string msg = "Indice de clorofila de la muestra es " + clorofila;
                Guide.BeginShowMessageBox("Clorofila", msg, MBOPTIONS, 0, MessageBoxIcon.Alert, null, null);

                ProcessButton3.IsEnabled = true;
            }
        }

        private void Button_Click4(object sender, RoutedEventArgs e)
        {
            ProcessButton3.IsEnabled = false;
            BitmapImage bi = new BitmapImage();

                if (numImagen == 4)
                {
                    numImagen = 1;
                }
                else {
                    numImagen++;
                }

                switch (numImagen)
                {
                    case 1:
                        bi.UriSource = new Uri("Assets\\marcadores3.jpg", UriKind.Relative);
                        break;
                    case 2:
                        bi.UriSource = new Uri("Assets\\marcadores4.jpg", UriKind.Relative);
                        break;
                    case 3:
                        bi.UriSource = new Uri("Assets\\marcadores5.jpg", UriKind.Relative);
                        break;
                    case 4:
                        bi.UriSource = new Uri("Assets\\marcadores6.jpg", UriKind.Relative);
                        break;
                };

            Preview.Source = bi;
            ProcessButton3.IsEnabled = true;
        }

    }
}