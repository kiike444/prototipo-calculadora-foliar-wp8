﻿using OpenCVComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace AplicacionBiologia.Util
{



    class CalculosBiologia
    {
        public static int FORMULA_1 = 0;   // ((((r + b) / 255) - ((r - b) / 255)) / 3)
        public static int FORMULA_2 = 1;   // b / (r + b + g)
        public static int FORMULA_3 = 2;   // g / (r + b + g)
        public static int FORMULA_4 = 3;   // r /  (r + b + g)
        public static int FORMULA_5 = 4;   // g / r
        private static OpenCVLib opencv = new OpenCVLib();


        public static double calcularArea(ImageSource imagen) {
            WriteableBitmap bitmap = new WriteableBitmap(imagen as BitmapSource);
            double area = opencv.calcularArea(bitmap.Pixels, bitmap.PixelWidth, bitmap.PixelHeight);
            return (double)Decimal.Round(new Decimal(area), 2);
        }

        public static double calcularClorofila(ImageSource imagen, int formula)
        {
            WriteableBitmap bitmap = new WriteableBitmap(imagen as BitmapSource);
            double clorofila = opencv.calcularClorofila(bitmap.Pixels, bitmap.PixelWidth, bitmap.PixelHeight,formula);
            return (double)Decimal.Round(new Decimal(clorofila), 2);
        }

        public async static Task<ImageSource> aplicarThreshold( ImageSource imagen, int umbral)
        {
            WriteableBitmap bitmap = new WriteableBitmap(imagen as BitmapSource);
            var pixeles = await opencv.aplicarThreshold(bitmap.Pixels, bitmap.PixelWidth, bitmap.PixelHeight, umbral);
            //copiar los pixeles recibidos de opencv a la imagen
            for (int x = 0; x < bitmap.Pixels.Length; x++)
            {
                bitmap.Pixels[x] = pixeles[x];
            }
            return bitmap;
        }

    }
}
