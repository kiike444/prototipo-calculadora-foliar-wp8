#include "pch.h"
#include "OpenCVComponent.h"
#include <opencv2\imgproc\types_c.h>
#include <opencv2\core\core.hpp>
#include <opencv2\imgproc\imgproc.hpp>
#include <vector>
#include <algorithm>

using namespace cv;
using namespace std;
using namespace OpenCVComponent;
using namespace Platform;
using namespace concurrency;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;


namespace OpenCVComponent
{

	class Calculos
	{
	public:
		Calculos(){}

		/*
		Obtiene el �rea de una imagen a partir de los pixeles del marcador y la muestra encontrados en la imagen
		Parametros:
			IVector<int> input - Los pixeles de la imagen
			int width - el ancho de la imagen
			int height - lo alto de la imagen
		Regresa:
			double area - El �rea de la muestra
		*/
		double calcularArea(Windows::Foundation::Collections::IVector<int>^ input, int width, int height){
			int size = input->Size;
			double area = 0;
			double referencia = 0;
			double muestra = 0;
			double alturaAreaDeMarcador = (height / 4);

			cv::Mat imagen(width, height, CV_8UC4);
			CopyIVectorToMatrix(input, imagen, size);

			//Aplicamos filtros a la imagen
			cv::Mat intermediateMat, intermediateMat2, imagen2;
			cv::cvtColor(imagen, intermediateMat, CV_BGR2RGB);
			cv::cvtColor(intermediateMat, intermediateMat2, CV_RGB2GRAY);
			cv::threshold(intermediateMat2, imagen2, 170, 255, THRESH_BINARY);

			for (int i = 0; i < imagen2.size().height; i++){
				for (int j = 0; j < imagen2.size().width; j++){
					uchar bgrPixel = imagen2.at<uchar>(i, j);
					if (i < alturaAreaDeMarcador){
						if (bgrPixel == 0){
							referencia++;
						}
					}
					else{
						if (bgrPixel == 0){
							muestra++;
						}
					}
				}
			}

			area = muestra / referencia;
			return area;
		}


		/*
		Aplica el filtro threshold de OpenCV a una imagen
		Parametros:
			IVector<int> input - Los pixeles de la imagen
			int width - el ancho de la imagen
			int height - lo alto de la imagen
			int umbral - grado del umbral para aplicar en el threshold
		Regresa:
			IVectorView<int>^>^ imagenVector - Los pixeles de la imagen ya con el filtro aplicado
		*/
		Windows::Foundation::IAsyncOperation<Windows::Foundation::Collections::IVectorView<int>^>^ aplicarThreshold(Windows::Foundation::Collections::IVector<int>^ input, int width, int height, int umbral){
			int size = input->Size;
			cv::Mat imagen(width, height, CV_8UC4);
			CopyIVectorToMatrix(input, imagen, size);

			return create_async([=]() -> IVectorView<int>^
			{
				// convert to grayscale
				cv::Mat intermediateMat, intermediateMat2;
				cv::cvtColor(imagen, intermediateMat, CV_BGR2GRAY);
				cv::threshold(intermediateMat, intermediateMat2, 170, 255, THRESH_BINARY_INV);
				cv::cvtColor(intermediateMat2, imagen, CV_GRAY2BGRA);


				//int widthMarcador = (width / 4)*1.6;
				//int heigthMarcador = (height / 4)*1.6;
				//rectangle(imagen, cv::Point(0, 0), cv::Point(widthMarcador, heigthMarcador), CV_RGB(0, 0, 255), 3, 8, 0);	//Dibujamos el rectangulo 

				std::vector<int> imagenVector;
				CopyMatrixToVector(imagen, imagenVector, size);

				// Return the outputs as a VectorView<float>
				return ref new Platform::Collections::VectorView<int>(imagenVector);
			});
		}



		/*
		Calcula el �ndice de clorofila de una imagen basandose en los valores RGB de cada pixel
		Parametros:
			IVector<int> input - Los pixeles de la imagen
			int width - el ancho de la imagen
			int height - lo alto de la imagen
			formula - El n�mero que identifica a la formula que se va a emplear
		Regresa:
			El indice de clorofila
			-1 - Si ocurre un error de f�rmula
		*/
		double calcularClorofila(Windows::Foundation::Collections::IVector<int>^ input, int width, int height, int formula){
			int size = input->Size;
			double r = 0;
			double g = 0;
			double b = 0;
			int numpixeles = 0;

			cv::Mat imagen(width, height, CV_8UC4), imagen2;
			CopyIVectorToMatrix(input, imagen, size);

			//Aplicamos los filtros
			cv::cvtColor(imagen, imagen2, CV_BGR2RGB);

			//Accedemos a cada pixel de la imagen y sumamos sus valores RGB
			for (int i = 0; i < imagen2.rows; i++){
				for (int j = 0; j < imagen2.cols; j++){
					Vec3b bgrPixel = imagen2.at<Vec3b>(i, j);

					if (!( (bgrPixel.val[0] == 255 && bgrPixel.val[1] == 255 && bgrPixel.val[2] == 255) || 
						(bgrPixel.val[0] == 0 && bgrPixel.val[1] == 0 && bgrPixel.val[2] == 0))){
						//rojo
						if (bgrPixel.val[0] != 0){
							r = r + bgrPixel.val[0];
						}
						//verde
						if (bgrPixel.val[1] != 0){
							g = g + bgrPixel.val[1];
						}
						//azul
						if (bgrPixel.val[2] != 0){
							b = b + bgrPixel.val[2];
						}
						numpixeles++;
					}
				}
			}

			//Obtenemos un promedio en los tres canales de color (RGB) de la imagen
			r = r / numpixeles;
			g = g / numpixeles;
			b = b / numpixeles;

			//Aplicamos la f�rmula para calcular la clorofila
			/*
				FORMULA_1 = 0 
				FORMULA_2 = 1 
				FORMULA_3 = 2
				FORMULA_4 = 3
				FORMULA_5 = 4
			*/
			double clorofila = 0;
			switch (formula){
			case FORMULA_1:
				clorofila = ((((r + b) / 255) - ((r - b) / 255)) / 3);
				break;
			case FORMULA_2:
				clorofila = b / (r + b + g);
				break;
			case FORMULA_3:
				clorofila = g / (r + b + g);
				break;
			case FORMULA_4:
				clorofila = r / (r + b + g);
				break;
			case FORMULA_5:
				clorofila = g / r;
				break;
			default:
				clorofila = -1;
				break;
			}

			return clorofila;
		}


		/*
			Convierte un vector de pixeles a una matriz
		*/
		void CopyIVectorToMatrix(IVector<int>^ input, cv::Mat& mat, int size)
		{
			unsigned char* data = mat.data;
			for (int i = 0; i < size; i++)
			{
				int value = input->GetAt(i);
				memcpy(data, (void*)&value, 4);
				data += 4;
			}
		}

		/*
			convierte una matriz de pixeles a un vector de pixeles
		*/
		void CopyMatrixToVector(const cv::Mat& mat, std::vector<int>& vector, int size)
		{
			int* data = (int*)mat.data;
			for (int i = 0; i < size; i++)
			{
				vector.push_back(data[i]);
			}

		}
	};
}