﻿// OpenCVComponent.cpp
#include "pch.h"
#include "OpenCVComponent.h"
#include "CalculosBiologia.h"
#include <vector>
#include <algorithm>

using namespace cv;

using namespace OpenCVComponent;
using namespace Platform;
using namespace concurrency;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;

void CopyIVectorToMatrix(IVector<int>^ input, cv::Mat& mat, int size);
void CopyMatrixToVector(const cv::Mat& mat, std::vector<int>& vector, int size);


OpenCVLib::OpenCVLib()
{
}


double  OpenCVLib::calcularArea(IVector<int>^ input, int width, int height){
	Calculos calculosBiologia;
	return calculosBiologia.calcularArea(input, width, height);

}

IAsyncOperation<IVectorView<int>^>^ OpenCVLib::aplicarThreshold(IVector<int>^ input, int width, int height, int umbral){
	Calculos calculosBiologia;
	return calculosBiologia.aplicarThreshold(input, width, height, umbral);
}

double OpenCVLib::calcularClorofila(Windows::Foundation::Collections::IVector<int>^ input, int width, int height, int formula){
	Calculos calculosBiologia;
	return calculosBiologia.calcularClorofila(input, width, height, formula);
}


