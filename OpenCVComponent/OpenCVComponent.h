﻿#pragma once

#include <ppltasks.h>
#include <collection.h>

namespace OpenCVComponent
{

	enum {
		FORMULA_1 = 0, // ((((r + b) / 255) - ((r - b) / 255)) / 3)
		FORMULA_2 = 1, // b / (r + b + g)
		FORMULA_3 = 2, // g / (r + b + g)
		FORMULA_4 = 3, // r / (r + b + g)
		FORMULA_5 = 4 // g / r
	};

    public ref class OpenCVLib sealed
    {
    public:
        OpenCVLib();
		double calcularClorofila(Windows::Foundation::Collections::IVector<int>^ input, int width, int height, int formula);
		double calcularArea(Windows::Foundation::Collections::IVector<int>^ input, int width, int height);
		Windows::Foundation::IAsyncOperation<Windows::Foundation::Collections::IVectorView<int>^>^ aplicarThreshold(Windows::Foundation::Collections::IVector<int>^ input, int width, int height, int umbral);
	};
}