# Foliar Windows Phone#

Prototipo de una aplicación móvil que calcula el área foliar y el índice de clorofila de las hojas de las plantas.

* Versión 2.0

### Dependencias ###

* OpenCV support for Windows Universal Apps (https://msopentech.com/blog/2014/08/20/opencv-support-for-windows-universal-apps-now-available-on-github/)

### Requerimientos de los dispositivos ###

* Windows Phone 8 o superior